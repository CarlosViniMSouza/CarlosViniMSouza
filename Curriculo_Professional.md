# Currículo Profissional

## [👨‍💻 Carlos Vinícius Monteiro de Souza](https://github.com/CarlosViniMSouza/CarlosViniMSouza/blob/main/README.md)
#### [🔎 Endereço - Manaus - Amazonas](https://www.google.com/maps/place/R.+Epaminondas+Bara%C3%BAna,+32-10+-+Parque+10+de+Novembro,+Manaus+-+AM,+69054-714/@-3.0734622,-60.0067465,19z/data=!3m1!4b1!4m13!1m7!3m6!1s0x926c1a8c5a4e3fa9:0xdee7e50cb28e5a27!2sR.+Epaminondas+Bara%C3%BAna+-+Parque+Dez+de+Novembro,+Manaus+-+AM,+69055-010!3b1!8m2!3d-3.0737536!4d-60.0050479!3m4!1s0x926c1a8c5a4ca84b:0x56402a17937791bd!8m2!3d-3.0734673!4d-60.006201)
#### [🔵 Contato via-Telegram](https://t.me/CarlosViniMSouza) [🟢 Contato via-Whatssap](https://api.whatsapp.com/send?phone=5592992680331) [ 🌐 Contato via-LinkedIn](https://www.linkedin.com/in/carlos-souza-technology/)


## 🎯 _Objetivo Profissional_
```R
Cargo na área de Ciência de Dados e/ou Desenvolvimento de Software:
° Cientista de Dados Jr.
° Desenvolvedor Python Jr.
```

## 📚 _Formação_
```R
° Graduação em Engenharia de Software - Em Andamento.
    ° Instituto Federal do Amazonas.
    ° Conclusão: Dezembro/2024.

° Graduação em Enganharia Elétrica - Cancelado.
    ° Universidade do Estado do Amazonas.
    ° Início: Março/2019.
    ° Cancelado: Novembro/2020.
```

## 🗺 _Idiomas_
```R
Inglês – nível Intermediário.
Espanhol – nível Intermediário.
Italiano – nível Básico.
```

## ⚒ _Atividades Extracurricular_
### ° Organização do Festival de Dança durante o Ensino Médio.
### ° Participação em Olimpíadas de Matemática:
```R
	1 – OBMEP;
	2 – OAM;
	3 – Olimpíada de Matemática do SESI;
	4 – Olimpíada de Matemática da EST – UEA;
```

### ° Participação em cursos técnicos da [Samsung Ocean](http://www.oceanbrasil.com/):
```R
	1 – Web Scraping com Python;
	2 – Ciência de Dados: Laboratório com Pandas e Python;
	3 – Deep Learning: Introdução com Keras e Python;
	4 – Ciência de Dados Aplicada ao Setor Público;
	5 – Uso de dados de Mídia Social para Análise Urbana;
```

### ° Participação em Hackathons e Competições, onde exerci funções e atividades como:
```R
	1 – IBM Behind The Code – Edition 2020:
	Desenvolvedor autônomo com ênfase em:
		1.1 – Reconhecimento de imagens e entidades textuais com Inteligência Artificial;
		1.2 – Desenvolvimento de aplicações Web com Cloud Foundry.
		1.3 – ferramentas de DevOps;
		1.4 – Ciência de Dados;
		1.5 – Feature Engineering;
		1.6 – Desenvolvimento e deployment de modelos de Aprendizado de Máquina com Jupyter 
		Notebook, Watson Machine Learning e SPSS Modeler(todos são serviços disponibilizados na IBM Cloud);
		1.7 – Desenvolvimento de Assistentes Virtuais inteligentes integrados com APIs e Microsserviços;
```
```R
	2 – Hackathon Eugenio&Natura:
	Desenvolvedor Back-End, atividade exercida:
		### ° Desenvolver o Back-End da aplicação que, ao receber algumas informações do usuário – que serão 
		guardadas dentro de um Banco de Dados SQL – deverá agilizar o processo de compra e recomendação de 
		produtos (com base nas preferências de diversas pessoas). Utilizando de APIs da Eugenio(para análise 
		de dados) e Google Maps(na parte de Geolocalização do aplicativo).
```
```R
	3 – Hackathon GetNet&SEBRAE:
	Desenvolvedor Back-End, atividade exercida:
		### °  Desenvolver o Back-End&I.A. da aplicação que, ao receber algumas informações do usuário – que serão
		guardadas dentro de um Banco de Dados NoSQL – poderá fornecer suporte em: indicação de linhas de crédito
		(para a situação atual de sua microempresa), direcionamento para cursos e especializações necessárias á 
		atividade essencial da empresa, e também, fornecer para Bancos e Instituições de Apoio ao Pequeno e Médio
		Empreendedor, informações relevantes sobre seus clientes e ‘potenciais’ clientes.
```