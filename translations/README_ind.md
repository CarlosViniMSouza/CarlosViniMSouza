#### Veja esse README.md em Português : <kbd>[<img title="Brasil" alt="Brasil" src="https://github.com/CarlosViniMSouza/CarlosViniMSouza/blob/main/flags/br.jpg" width="22">](https://github.com/CarlosViniMSouza/CarlosViniMSouza/blob/main/README.md)</kbd>

<h2> हाय दोस्तों, मैं कार्लोस विनीसियस मोंटेइरो डी सूजा हूँ। <img src="https://github.com/souvikguria98/souvikguria98/blob/master/Hi.gif" width="25"></h2>

<h3> 📡&nbsp; यदि आप मुझसे संपर्क करना चाहते हैं, तो चैट करें या केवल एक विचार का आदान-प्रदान करें, यहां कुछ तरीके उपलब्ध हैं : </h3>

&nbsp; <a href="https://twitter.com/CarlosViniMS1/" target="_blank" rel="noopener noreferrer"><img src="https://img.icons8.com/clouds/90/4a90e2/twitter.png"/></a>
&nbsp; <a href="https://www.instagram.com/CarlosViniMSouza/" target="_blank" rel="noopener noreferrer"><img src="https://img.icons8.com/clouds/90/4a90e2/instagram-new--v2.png"/></a>
&nbsp; <a href="https://www.linkedin.com/in/carlos-souza-technology/" target="_blank" rel="noopener noreferrer"><img src="https://img.icons8.com/clouds/90/4a90e2/linkedin.png"/></a>
&nbsp; <a href="mailto:vinicius.souza5530@gmail.com" target="_blank" rel="noopener noreferrer"><img src="https://img.icons8.com/clouds/90/4a90e2/gmail.png"/></a>
&nbsp; <a href="https://www.facebook.com/profile.php?id=100066752509880" target="_blank" rel="noopener noreferrer"><img src="https://img.icons8.com/clouds/90/000000/facebook-new.png"/></a>
&nbsp; <a href="https://app.rocketseat.com.br/me/carlos-vinicius-monteiro-de-souza-05677" target="_blank" rel="noopener noreferrer"><img src="https://img.icons8.com/clouds/90/4a90e2/rocket.png"/></a>
&nbsp; <a href="https://t.me/CarlosViniMSouza" target="_blank" rel="noopener noreferrer"><img src="https://img.icons8.com/clouds/90/000000/telegram-app.png"/></a>
&nbsp; <a href="https://api.whatsapp.com/send?phone=5592992680331" target="_blank" rel="noopener noreferrer"><img src="https://img.icons8.com/clouds/90/000000/whatsapp.png"/></a>
  
<h2> 🌐&nbsp; कैसे के बारे में हम एक अच्छा लेते हैं ☕&nbsp; और NetWork करते हैं! ✌️🤓&nbsp; </h2>

<h3> 👨‍💼💻&nbsp; मेरे बारे मेँ : </h3>

### - 📖&nbsp; डेटा साइंस और बिजनेस इंटेलिजेंस के लिए आर और पायथन का अध्ययन;
### - 📚&nbsp; पर केंद्रित: आर्टिफिशियल इंटेलिजेंस, मशीन लर्निंग, बिग डेटा;
### - 🗺&nbsp; भाषाएँ: अंग्रेजी (इंटरमीडिएट), स्पेनिश (इंटरमीडिएट), इतालवी (बेसिक);
### - 🎓&nbsp; Amazonas के संघीय संस्थान में सॉफ्टवेयर इंजीनियरिंग का अध्ययन;
### - 🛠&nbsp; मेरे T.I करियर की नींव बनाना;
### - 🖖&nbsp; मैं किसी भी देव के साथ प्रौद्योगिकी कार्यक्रमों और नेटवर्क में भाग लेना पसंद करता हूं;
### - 🖥&nbsp; शौक: खेल, एक्शन मूवी और सीखना कुछ नया;
### - ☕&nbsp; मेरे लिए कॉफी कुछ बहुत महत्वपूर्ण है (समाप्त परियोजना को देखने से बेहतर है, यह एक अच्छी कॉफी है);

<h3>.</h3>

<h3> :electron:&nbsp; तकनीकी कौशल : </h3>

### - 💻&nbsp; प्रोग्रामिंग की भाषाएँ
<img alt="Python" src="https://img.shields.io/badge/-Python-007ACC?style=flat-square&logo=Python&logoColor=white" href="https://www.python.org/"/> |
<img alt="R" src="https://img.shields.io/badge/-R-2088FF?style=flat-square&logo=R&logoColor=white" href="https://www.r-project.org/"/> |
<img alt="JavaScript" src="https://img.shields.io/badge/-JavaScript-F7B93E?style=flat-square&logo=JavaScript&logoColor=white" href="https://www.javascript.com/"/> |
<img alt="Kotlin" src="https://img.shields.io/badge/-Kotlin-E10098?style=flat-square&logo=Kotlin&logoColor=white" href="https://kotlinlang.org/"/> |
<img alt="C++" src="https://img.shields.io/badge/-C++_Language-232F3E?style=flat-square&logo=C%2B%2B&logoColor=white" href="https://docs.microsoft.com/en-us/cpp/?view=msvc-160"/> |
<img alt="C" src="https://img.shields.io/badge/-C_Language-232F3E?style=flat-square&logo=C&logoColor=white" href="https://docs.microsoft.com/en-us/cpp/?view=msvc-160"/> |
<img alt="Java" src="https://img.shields.io/badge/Java-%23007396.svg?logo=java&logoColor=white" href="https://www.java.com/pt-BR/"/> |
<img alt="HTML" src="https://img.shields.io/badge/HTML%20-%23E34F26.svg?logo=html5&logoColor=white" href="" /> |
<img alt="CSS" src="https://img.shields.io/badge/CSS%20-%231572B6.svg?logo=css3&logoColor=white" href=""/> |
<img alt="MarkDown" src="https://img.shields.io/badge/Markdown-%23000000.svg?logo=markdown&logoColor=white" href="https://markdown.net.br/"/> |
<img alt="MarkDown" src="https://img.shields.io/badge/Node.js%20-%2343853D.svg?logo=node-dot-js&logoColor=white" href="https://nodejs.org/en/download/"/> |

### - 🌐&nbsp; पुस्तकालय और उपकरण 
<img alt="Packages R" src="https://img.shields.io/badge/-Packages_R-2088FF?style=flat-square&logo=R&logoColor=white" href="https://cloud.r-project.org/"/> | 
<img alt="TypeScript" src="https://img.shields.io/badge/-TypeScript-007ACC?style=flat-square&logo=typescript&logoColor=white" href="https://www.typescriptlang.org/"/> |
<img alt="Apache Spark" src="https://img.shields.io/badge/-Apache_Spark-FB542B?style=flat-square&logo=apache-spark&logoColor=white" href="https://spark.apache.org/"/> | 
<img alt="GitHub Actions" src="https://img.shields.io/badge/GitHub%20Actions%20-%232671E5.svg?logo=github%20actions&logoColor=white" href="https://docs.github.com/en/actions"/> | 
<img alt="Numpy" src="https://img.shields.io/badge/Numpy%20-%23013243.svg?logo=numpy&logoColor=white" href="https://numpy.org/"/> | 
<img alt="Pandas" src="https://img.shields.io/badge/Pandas%20-%23150458.svg?logo=pandas&logoColor=white" href="https://pandas.pydata.org/docs/user_guide/index.html#user-guide"/> | 
<img alt="Seaborn" src="https://seaborn.pydata.org/_static/logo-wide-lightbg.svg" href="https://seaborn.pydata.org/#" width="60"/> | 
<img alt="TensorFlow" src="https://img.shields.io/badge/TensorFlow%20-%23FF6F00.svg?logo=TensorFlow&logoColor=white" href="https://www.tensorflow.org/"/> | 
<img alt="Selenium" src="https://img.shields.io/badge/Selenium%20-%2302569B.svg?logo=Selenium&logoColor=white" href="https://www.selenium.dev/selenium-ide/"/> | 
<img alt="Selenium" src="https://img.shields.io/badge/Selenium%20-%2302569B.svg?logo=Selenium&logoColor=white" href="https://www.selenium.dev/selenium-ide/"/> |
<img alt="Plotly" src="https://img.shields.io/badge/Plotly%20-21759B.svg?logo=Plotly&logoColor=white" href="https://plotly.com/"/> |
<img alt="Postman" src="https://img.shields.io/badge/Postman-FF6C37?logo=postman&logoColor=white" href="https://www.postman.com/"/> |
<img alt="Power BI" src="https://img.shields.io/badge/Power_BI-%23E39842.svg?logo=Power%20BI" href="https://powerbi.microsoft.com/pt-br/what-is-power-bi/"/> |
<img alt="Terminal Linux" src="https://img.shields.io/badge/Terminal_Linux%20-%23121011.svg?logo=gnu-bash&logoColor=white" href="" /> |
<img alt="Git" src="https://img.shields.io/badge/Git%20-%23F05033.svg?logo=git&logoColor=white" href="https://git-scm.com/" /> |

### - 🛢&nbsp; डेटाबेस और क्लाउड सेवाएं
<img alt="Azure" src="https://img.shields.io/badge/-Microsoft_Azure-45b8d8?style=flat-square&logo=microsoft-azure&logoColor=white" href="https://azure.microsoft.com/pt-br/"/> | 
<img alt="MongoDB" src="https://img.shields.io/badge/-MongoDB-13aa52?style=flat-square&logo=mongodb&logoColor=white" href="https://www.mongodb.com/"/> | 
<img alt="SQL Server" src="https://img.shields.io/badge/-SQL_Server_Microsoft-46a2f1?style=flat-square&logo=Microsoft-SQL-Server&logoColor=white" href="https://www.microsoft.com/pt-br/sql-server"/> | 
<img alt="Visual Studio" src="https://img.shields.io/badge/-Visual_Studio_Microsoft-3b2e5a?style=flat-square&logo=Visual-Studio&logoColor=white" href="https://visualstudio.microsoft.com/pt-br/"/> |
<img alt="IBM Cloud" src="https://img.shields.io/badge/IBM_Cloud-21759B?logo=ibm&logoColor=white" href="https://cloud.ibm.com/login"/> |
<img alt="PostgreSQL" src="https://img.shields.io/badge/PostgreSQL-%23316192.svg?logo=postgresql&logoColor=white" href="https://www.postgresql.org/"/> |

### - ⚒&nbsp; एकीकृत विकास वातावरण - IDE's
<img alt="Anaconda" src="https://img.shields.io/badge/-Anaconda-13aa52?style=flat-square&logo=anaconda&logoColor=white" href="https://www.anaconda.com/blog"/> | 
<img alt="RStudio" src="https://img.shields.io/badge/-RStudio-8DD6F9?style=flat-square&logo=rstudio&logoColor=white" href="https://www.rstudio.com/"/> | 
<img alt="VS Code" src="https://img.shields.io/badge/-VS_Code-45b8d8?style=flat-square&logo=visual-studio-code&logoColor=white" href="https://code.visualstudio.com/"/> | 
<img alt="Intellij Idea" src="https://img.shields.io/badge/-IntelliJ_IDEA-3b2e5a?style=flat-square&logo=IntelliJ-IDEA&logoColor=white" href="https://www.jetbrains.com/idea/"/> | 
<img alt="PyCharm" src="https://img.shields.io/badge/-PyCharm-003f2c?style=flat-square&logo=PyCharm&logoColor=white" href="https://www.jetbrains.com/pycharm/"/> | 
<img alt="Eclipse" src="https://img.shields.io/badge/-Eclipse-3b2e5a?style=flat-square&logo=Eclipse&logoColor=white" href="https://www.eclipse.org/ide/"/> |
<img alt="Android Studio" src="https://img.shields.io/badge/Android%20Studio-008678.svg?logo=android-studio&logoColor=white" href="https://developer.android.com/studio"/> |
<img alt="NetBeans" src="https://img.shields.io/badge/-NetBeans-45b8d8?style=flat-square&logo=apache-netbeans-ide&logoColor=white" href="https://netbeans.apache.org/"/> |
<img alt="Jupyter Notebook" src="https://img.shields.io/badge/-Jupyter_Notebook-FB542B?style=flat-square&logo=Jupyter&logoColor=white" href="https://jupyter.org/index.html"/> |

<h3>.</h3>

<h3> 🧑‍🔬&nbsp; प्रोग्रामिंग में मेरी प्रगति :  </h3>

[![GitHub Streak](http://github-readme-streak-stats.herokuapp.com/?user=CarlosViniMSouza&theme=dracula&show_icons=true)](https://github.com/DenverCoder1/github-readme-streak-stats)

![CarlosViniMSouza GitHub stats](https://github-readme-stats.vercel.app/api?username=CarlosViniMSouza&layout=compact&theme=dracula&count_private=true&include_all_commits=true&show_icons=true)

![CarlosViniMSouza Wakatime stats](https://github-readme-stats.vercel.app/api/wakatime?username=CarlosViniMSouza&layout=compact&theme=dracula&range=last_7_days)

![Top Langs](https://github-readme-stats.vercel.app/api/top-langs/?username=CarlosViniMSouza&layout=compact&theme=dracula&langs_count=10&hide=jupyter%20notebook,Java,c%2B%2B,TSQL)

<h3>.</h3>

<h3> 🗃&nbsp; मेरा योगदान : </h3>

[![ReadMe Card](https://github-readme-stats.vercel.app/api/pin/?username=iuricode&repo=recursos-gratuitos&theme=dracula)](https://github.com/iuricode/recursos-gratuitos)
[![ReadMe Card](https://github-readme-stats.vercel.app/api/pin/?username=gabrielcmarinho&repo=links-uteis&theme=dracula)](https://github.com/gabrielcmarinho/links-uteis)

[![ReadMe Card](https://github-readme-stats.vercel.app/api/pin/?username=Lorenalgm&repo=hackathon-dicas&theme=dracula)](https://github.com/Lorenalgm/hackathon-dicas)
[![ReadMe Card](https://github-readme-stats.vercel.app/api/pin/?username=geekcomputers&repo=Python&theme=dracula)](https://github.com/geekcomputers/Python)

[![ReadMe Card](https://github-readme-stats.vercel.app/api/pin/?username=perifacode&repo=conteudo-gratuito&theme=dracula)](https://github.com/perifacode/conteudo-gratuito)
[![ReadMe Card](https://github-readme-stats.vercel.app/api/pin/?username=dmpe&repo=R&theme=dracula)](https://github.com/dmpe/R)

[![ReadMe Card](https://github-readme-stats.vercel.app/api/pin/?username=EddieHubCommunity&repo=awesome-github-profiles&theme=dracula)](https://github.com/EddieHubCommunity/awesome-github-profiles)
[![ReadMe Card](https://github-readme-stats.vercel.app/api/pin/?username=kelvins&repo=Algoritmos-e-Estruturas-de-Dados&theme=dracula)](https://github.com/kelvins/Algoritmos-e-Estruturas-de-Dados)

![Activity Graph](https://activity-graph.herokuapp.com/graph?username=CarlosViniMSouza&theme=github)

<h3>.</h3>

<h3> 🏆&nbsp; मेरी गिटहब ट्राफियां : </h3>

[![trophy](https://github-profile-trophy.vercel.app/?username=CarlosViniMSouza&theme=dracula&no-frame=true&margin-w=15&row=2&column=4)](https://github-profile-trophy.vercel.app/?username=CarlosViniMSouza&theme=dracula)

<h3>.</h3>

🤗&nbsp; **शुकि्रया : [Duduxs](https://github.com/Duduxs), [devSouvik](https://github.com/devSouvik) e [Raven](https://github.com/Anirban166) - मेरे README.md के निर्माण में मदद के लिए**

**अतिरिक्त: आपके README.md को कस्टमाइज़ करने के लिए सुपर-कूल रिपॉजिटरी**

[![ReadMe Card](https://github-readme-stats.vercel.app/api/pin/?username=anuraghazra&repo=github-readme-stats&theme=dracula)](https://github.com/anuraghazra/github-readme-stats)
[![ReadMe Card](https://github-readme-stats.vercel.app/api/pin/?username=abhisheknaiidu&repo=awesome-github-profile-readme&theme=dracula)](https://github.com/abhisheknaiidu/awesome-github-profile-readme)

[![ReadMe Card](https://github-readme-stats.vercel.app/api/pin/?username=DenverCoder1&repo=github-readme-streak-stats&theme=dracula)](https://github.com/DenverCoder1/github-readme-streak-stats)
[![ReadMe Card](https://github-readme-stats.vercel.app/api/pin/?username=EddieHubCommunity&repo=awesome-github-profiles&theme=dracula)](https://github.com/EddieHubCommunity/awesome-github-profiles)

[![ReadMe Card](https://github-readme-stats.vercel.app/api/pin/?username=ryo-ma&repo=github-profile-trophy&theme=dracula)](https://github.com/ryo-ma/github-profile-trophy)

<h3>.</h3>

<details> 
  <summary> 📊&nbsp; मेरा गिटहब मेट्रिक्स : </summary>
  <br/>

<!-- If you're using "main" as default branch -->
![Metrics](https://github.com/CarlosViniMSouza/CarlosViniMSouza/blob/main/github-metrics.svg)

<br/>
</details>

<h3>.</h3>

<h3> 👋&nbsp; मेरी प्रोफ़ाइल पर विज़िट की संख्या : </h3>

![VisitorCount](https://profile-counter.glitch.me/CarlosViniMSouza/count.svg)

<!-- If you're using "main" as default branch -->
![GIF](https://raw.githubusercontent.com/CarlosViniMSouza/CarlosViniMSouza/output/github-contribution-grid-snake.svg)

[![forthebadge](https://forthebadge.com/images/badges/built-by-developers.svg)](https://forthebadge.com)
[![forthebadge](https://forthebadge.com/images/badges/compatibility-club-penguin.svg)](https://forthebadge.com)
[![forthebadge](https://forthebadge.com/images/badges/made-with-markdown.svg)](https://forthebadge.com)

[![forthebadge](https://forthebadge.com/images/badges/powered-by-coffee.svg)](https://forthebadge.com)
[![forthebadge](https://forthebadge.com/images/badges/open-source.svg)](https://forthebadge.com)
[![forthebadge](https://forthebadge.com/images/badges/60-percent-of-the-time-works-every-time.svg)](https://forthebadge.com)